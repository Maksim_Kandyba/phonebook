package phonebook.userInterface;

import phonebook.data.Contact;
import phonebook.data.Data;
import phonebook.data.PhoneNumber;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by max on 23.02.16.
 */
public abstract class Menu {
    protected List<String> items;
    protected static List<Menu> current = new ArrayList<>();
    protected static List<Menu> next = new ArrayList<>();
    protected static String command;
    protected static Data data;
    protected static boolean workIsOver = false;

    public static void setData(Data data) {
        Menu.data = data;
    }

    public static void addNext(Menu menu) { next.add(menu); }

    public static void mainCycle(){
        Scanner scanner = new Scanner(System.in);
        do {
            current.clear();
            current.addAll(next);
            next.clear();
            System.out.println();
            for (Menu menu:current) menu.display();
            boolean processed = false;
            do {
                command = scanner.nextLine();
                for (Menu menu:current){
                    processed = menu.processCommand();
                    if (processed) break;
                }
            } while (!processed);
        } while (!workIsOver);
    }

    private void display(){
        for (String item:items){
            System.out.println(item);
        }
    }

    public abstract boolean processCommand();

    protected void setEmail(Contact contact){
        System.out.println("Enter e-mail (to pass [p]):");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.nextLine();
        if (email.compareTo("p")!=0) contact.setEmail(email);
    }

    protected void setBirthday(Contact contact){
        System.out.println("Enter year of birth (to pass [0]):");
        Scanner scanner = new Scanner(System.in);
        try {
            int year = scanner.nextInt();
            if (year!=0){
                System.out.println("Enter month of birth:");
                int month = scanner.nextInt();
                System.out.println("Enter day of birth:");
                int day = scanner.nextInt();
                contact.setBirthday(LocalDate.of(year,month,day));
            }
        } catch (InputMismatchException e) {
            System.out.println("You shall enter numbers. Try again");
            setBirthday(contact);
        } catch (DateTimeException e) {
            System.out.println("You shall enter a real date. Try again");
            setBirthday(contact);
        }
    }

    protected void setAddress(Contact contact){
        System.out.println("Enter address (to pass [p]):");
        Scanner scanner = new Scanner(System.in);
        String address = scanner.nextLine();
        if (address.compareTo("p")!=0) contact.setAddress(address);
    }

    protected PhoneNumber.Type enterPhoneNumberType(String typeString){
        switch (typeString){
            case "h": return PhoneNumber.Type.HOME;
            case "w": return PhoneNumber.Type.WORK;
            case "p": return PhoneNumber.Type.PRIVATE;
            default: {
                System.out.println("Incorrect symbol. Type set as private");
                return PhoneNumber.Type.PRIVATE;
            }
        }
    }
}

package phonebook.userInterface;

import java.util.ArrayList;

/**
 * Created by max on 23.02.16.
 */
public class MenuA extends Menu {
    public MenuA() {
        items = new ArrayList<>();
        items.add("Display all contacts [d]");
    }

    @Override
    public boolean processCommand() {
        if (command.compareTo("d")==0){
                data.print();
                next.add(new MenuB());
                next.add(new MenuC());
                next.add(new QuitMenu());
                return true;
            }
        return false;
    }
}

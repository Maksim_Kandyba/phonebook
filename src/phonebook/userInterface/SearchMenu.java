package phonebook.userInterface;

import phonebook.data.Data;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by max on 23.02.16.
 */
public class SearchMenu extends Menu {
    public SearchMenu() {
        items = new ArrayList<>();
        items.add("Search by first name [f]");
        items.add("Search by last name [l]");
        items.add("Search by phone number [n]");
        items.add("Search by age [a]");
    }

    @Override
    public boolean processCommand() {
        Scanner scanner = new Scanner(System.in);
        int counter = 0;
        switch (command){
            case "f":{
                System.out.println("Enter a part of first name:");
                String firstNamePart = scanner.nextLine();
                counter = data.searchByName(firstNamePart, Data.NameType.FIRST);
            }
            break;
            case "l":{
                System.out.println("Enter a part of last name:");
                String lastNamePart = scanner.nextLine();
                counter = data.searchByName(lastNamePart, Data.NameType.LAST);
            }
            break;
            case "n":{
                System.out.println("Enter phone number:");
                String phoneNumber = scanner.nextLine();
                counter = data.searchByPhoneNumber(phoneNumber);
            }
            break;
            case "a":{
                System.out.println("Enter minimal age:");
                int minAge, maxAge;
                try {
                    minAge = scanner.nextInt();
                    System.out.println("Enter maximal age:");
                    maxAge = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("You shall enter a number");
                    return processCommand();
                }
                counter = data.searchByAge(minAge,maxAge);
            }
            break;
            default: return false;
        }
        if (data.getNumberOfContacts()!=0) next.add(new MenuA());
        next.add(new MenuB());
        if (counter > 0) next.add(new MenuC());
        next.add(new QuitMenu());
        return true;
    }
}

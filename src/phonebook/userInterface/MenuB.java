package phonebook.userInterface;

import phonebook.data.Contact;
import phonebook.data.PhoneNumber;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by max on 23.02.16.
 */
public class MenuB extends Menu {
    public MenuB() {
        items = new ArrayList<>();
        items.add("Search contacts [s]");
        items.add("Create new contact [c]");
        items.add("Import contact(s) [i]");
    }

    @Override
    public boolean processCommand(){
        switch (command){
            case "s":{
                next.add(new SearchMenu());
                return true;
            }
            case "c":{
                System.out.println("Enter first name:");
                Scanner scanner = new Scanner(System.in);
                String firstName = scanner.nextLine();
                System.out.println("Enter last name:");
                String lastName = scanner.nextLine();
                List<PhoneNumber> phoneNumbers = new ArrayList<>();
                do {
                    boolean ok;
                    String digits;
                    do {
                        System.out.println("Enter digits of phone number:");
                        digits = scanner.nextLine();
                        ok = !data.getContactsByPhoneNumber().containsKey(digits);
                        if (ok){
                            for (PhoneNumber phoneNumber:phoneNumbers){
                                if (phoneNumber.getDigits().compareTo(digits)==0){
                                    ok = false;
                                    break;
                                }
                            }
                        }
                        if (!ok) System.out.println
                                ("You have already had contact with such phone number");
                    } while (!ok);
                    System.out.println
                            ("Enter type of phone number: home [h], work [w], private [p]:");
                    String typeString = scanner.nextLine();
                    PhoneNumber.Type type = enterPhoneNumberType(typeString);
                    phoneNumbers.add(new PhoneNumber(digits,type));
                    System.out.println("Do you want to enter next phone number? (no [n])");
                } while (scanner.nextLine().compareTo("n")!=0);
                Contact contact = new Contact(firstName,lastName,phoneNumbers);
                setEmail(contact);
                setBirthday(contact);
                setAddress(contact);
                data.addContact(contact);
                data.setSorted(false);
                System.out.println("New contact has been added");
            }
                break;
            case "i":{
                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter file name to import from:");
                File file = new File(scanner.nextLine());
                try(
                        FileReader fr = new FileReader(file);
                ) {
                    scanner = new Scanner(fr);
                    int contactsBefore = data.getNumberOfContacts();
                    while (scanner.hasNextLine()){
                        String[] fields = scanner.nextLine().split(";");
                        Integer size;
                        try {
                            size = new Integer(fields[2]);
                        } catch (NumberFormatException e) {
                            continue;
                        }
                        if (fields.length!=6+size*2) continue;
                        List<PhoneNumber> phoneNumbers = new ArrayList<>();
                        PhoneNumber.Type type;
                        for (int i=1; i<=size; i++){
                            switch (fields[2+i*2]) {
                                case "HOME":
                                    type = PhoneNumber.Type.HOME;
                                    break;
                                case "WORK":
                                    type = PhoneNumber.Type.WORK;
                                    break;
                                case "PRIVATE":
                                    type = PhoneNumber.Type.PRIVATE;
                                    break;
                                default:
                                    continue;
                            }
                            if (!data.getContactsByPhoneNumber().containsKey(fields[1+i*2]))
                                phoneNumbers.add(new PhoneNumber(fields[1+i*2],type));
                        }
                        if (phoneNumbers.size()==0) continue;
                        Contact contact = new Contact(fields[0],fields[1],phoneNumbers);
                        contact.setEmail(fields[3+size*2]);
                        if (fields[4+size*2].compareTo("null")!=0)
                            try {
                                contact.setBirthday(LocalDate.parse(fields[4+size*2]));
                            } catch (DateTimeParseException e) {
                                continue;
                            }
                        contact.setAddress(fields[5+size*2]);
                        data.addContact(contact);
                        data.setSorted(false);
                    }
                    StringBuilder sb = new StringBuilder();
                    int counter = data.getNumberOfContacts()-contactsBefore;
                    sb.append(counter).append(" contact")
                            .append(counter==1?" was":"s were")
                            .append(" imported");
                    System.out.println(sb);
                } catch (IOException e) {
                    System.out.println("Can't import contact(s) from file");
                }
            }
                break;
            default: return false;
        }
        if (data.getNumberOfContacts()!=0) next.add(new MenuA());
        next.add(new MenuB());
        next.add(new QuitMenu());
        return true;
    }
}

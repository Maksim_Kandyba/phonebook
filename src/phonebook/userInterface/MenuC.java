package phonebook.userInterface;

import phonebook.data.Contact;
import phonebook.data.PhoneNumber;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by max on 27.02.16.
 */
public class MenuC extends Menu {
    public MenuC() {
        items = new ArrayList<>();
        items.add("Update contact [u]");
        items.add("Remove contact [r]");
        items.add("Export contact(s) [e]");
    }

    @Override
    public boolean processCommand() {
        Scanner scanner = new Scanner(System.in);
        switch (command){
            case "u":{
                System.out.println("Enter contact # to update:");
                int id;
                try {
                    id = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("You shall enter a number");
                    return processCommand();
                }
                Contact contact = data.getContact(id);
                if (contact==null){
                    System.out.println("There is no contact with such #");
                    break;
                }
                System.out.println("Enter new first name (to pass [p])");
                scanner = new Scanner(System.in);
                String firstName = scanner.nextLine();
                if (firstName.compareTo("p")!=0) contact.setFirstName(firstName);
                System.out.println("Enter new last name (to pass [p])");
                String lastName = scanner.nextLine();
                if (lastName.compareTo("p")!=0) contact.setLastName(lastName);
                StringBuilder sb;
                for (int i=0; i<contact.getPhoneNumbers().size(); i++){
                    sb = new StringBuilder();
                    sb.append("Enter new digits of ").append(i)
                            .append(" phone number (to pass [p]):");
                    String digits;
                    boolean ok;
                    do {
                        System.out.println(sb);
                        digits = scanner.nextLine();
                        if (digits.compareTo("p")==0) break;
                        ok = !data.getContactsByPhoneNumber().containsKey(digits);
                        if (ok){
                            for (PhoneNumber phoneNumber:contact.getPhoneNumbers()){
                                if (phoneNumber.getDigits().compareTo(digits)==0){
                                    ok = false;
                                    break;
                                }
                            }
                        }
                        if (!ok) System.out.println
                                ("You have already had contact with such phone number");
                    } while (!ok);
                    if (digits.compareTo("p")!=0){
                        data.getContactsByPhoneNumber()
                                .remove(contact.getPhoneNumbers().get(i).getDigits(),contact);
                        contact.getPhoneNumbers().get(i).setDigits(digits);
                        data.getContactsByPhoneNumber().put(digits,contact);
                    }
                    sb = new StringBuilder();
                    sb.append("Enter new type of ").append(i)
                            .append(" phone number: home [h], work [w], private [p] (to pass [0]):");
                    System.out.println(sb);
                    String typeString = scanner.nextLine();
                    if (typeString.compareTo("0")!=0){
                        PhoneNumber.Type type = enterPhoneNumberType(typeString);
                        contact.getPhoneNumbers().get(i).setType(type);
                    }
                }
                setEmail(contact);
                setBirthday(contact);
                setAddress(contact);
                data.setSorted(false);
                sb = new StringBuilder();
                sb.append("Contact # ").append(id).append(" has been updated");
                System.out.println(sb);
            }
            break;
            case "r":{
                System.out.println("Enter contact # to remove:");
                int id;
                try {
                    id = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("You shall enter a number");
                    return processCommand();
                }
                data.removeContact(id);
            }
            break;
            case "e":{
                System.out.println("Enter file name to export to:");
                File file = new File(scanner.nextLine());
                StringBuilder sb = new StringBuilder();
                sb.append("Enter contact(s) # to export separated by space ")
                        .append("or [a] to export the all:");
                System.out.println(sb);
                String string = scanner.nextLine();
                scanner = new Scanner(string);
                try(
                    FileWriter fw = new FileWriter(file);
                ) {
                    if (scanner.hasNextInt()){
                        while (scanner.hasNextInt())
                            fw.write(data.getContact(scanner.nextInt()).toString());
                        System.out.println("Contact(s) ha(s/ve) been exported");
                    } else {
                        if (string.compareTo("a")==0){
                            for (Contact contact:data.getContacts())
                                fw.write(contact.toString());
                            System.out.println("All contacts have been exported");
                        } else System.out.println("Incorrect parameter(s)");
                    }
                } catch (IOException e) {
                    System.out.println("Can't export contact(s) to file");
                }
            }
            break;
            default: return false;
        }
        if (data.getNumberOfContacts()!=0) next.add(new MenuA());
        next.add(new MenuB());
        next.add(new QuitMenu());
        return true;
    }
}

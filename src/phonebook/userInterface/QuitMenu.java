package phonebook.userInterface;

import java.util.ArrayList;

/**
 * Created by max on 27.02.16.
 */
public class QuitMenu extends Menu {
    public QuitMenu() {
        items = new ArrayList<>();
        items.add("Quit [q]");
    }
    @Override
    public boolean processCommand() {
        if (command.compareTo("q")==0) {
            workIsOver = true;
            return true;
        }
        return false;
    }
}

package phonebook.data;

import java.io.Serializable;

/**
 * Created by max on 20.02.16.
 */
public class PhoneNumber implements Serializable {
    private String digits;
    private Type type;

    public String getDigits() {
        return digits;
    }

    public Type getType() {
        return type;
    }

    public void setDigits(String digits) {
        this.digits = digits;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public PhoneNumber(String digits, Type type) {
        this.digits = digits;
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" phone number: ").append(digits).append(" (").append(type).append(")");
        return sb.toString();
    }

    public enum Type {
        HOME, WORK, PRIVATE
    }
}


package phonebook.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 20.02.16.
 */
public class Contact implements Serializable {
    private String firstName;
    private String lastName;
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();
    private String email;
    private LocalDate birthday;
    private String address;
    private int identifier = -1;

    public Contact(String firstName, String lastName, List<PhoneNumber> phoneNumbers) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumbers = phoneNumbers;
    }

    public int getIdentifier() {
        return identifier;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        StringBuilder sb = new StringBuilder();
        sb.append(firstName).append(" ").append(lastName);
        return sb.toString();
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(int id, String digits, PhoneNumber.Type type) {
        phoneNumbers.get(id).setDigits(digits);
        phoneNumbers.get(id).setType(type);
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumbers.add(phoneNumber);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void print(){
        StringBuilder sb = new StringBuilder();
        sb.append("\nContact # ").append(identifier)
        .append("\n").append(firstName).append(" ").append(lastName).append("\n");
        for (int i=0; i<phoneNumbers.size();i++){
            sb.append(i).append(phoneNumbers.get(i).toString()).append("\n");
        }
        sb.append("E-mail: ").append(email).append("\n")
                .append("Birthday: ").append(birthday).append("\n")
                .append("Address: ").append(address);
        System.out.println(sb);
    }

    private int computeAge() {
        if (birthday!=null){
            int result = LocalDate.now().getYear()-birthday.getYear();
            if (LocalDate.now().getDayOfYear()<birthday.getDayOfYear()) result -= 1;
            return result;
        }
        else return -1;
    }

    public int getAge(){
        return computeAge();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String sep = ";";
        sb.append(firstName).append(sep).append(lastName).append(sep)
                .append(phoneNumbers.size()).append(sep);
        for (PhoneNumber phoneNumber:phoneNumbers)
            sb.append(phoneNumber.getDigits()).append(sep)
                    .append(phoneNumber.getType()).append(sep);
        sb.append(email).append(sep).append(birthday).append(sep).append(address)
                .append("\n");
        return sb.toString();
    }
}

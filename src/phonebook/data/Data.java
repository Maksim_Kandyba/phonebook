package phonebook.data;

import java.io.Serializable;
import java.util.*;

/**
 * Created by max on 20.02.16.
 */
public class Data implements Serializable {
    private List<Contact> contacts = new LinkedList<>();
    private Map<String,Contact> contactsByPhoneNumber = new HashMap<>();
    private boolean sorted = false;
    private int contactsIdentifiersCounter = 0;

    public Contact getContact(int id) {
        for (Contact contact:contacts){
            if (contact.getIdentifier()==id) return contact;
        }
        return null;
    }

    public Map<String, Contact> getContactsByPhoneNumber() {
        return contactsByPhoneNumber;
    }

    public int getNumberOfContacts(){
        return contacts.size();
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    public void addContact(Contact contact){
        contacts.add(contact);
        contact.setIdentifier(contactsIdentifiersCounter++);
        for (PhoneNumber phoneNumber:contact.getPhoneNumbers()){
            contactsByPhoneNumber.put(phoneNumber.getDigits(),contact);
        }
        sorted = false;
    }

    public void removeContact(int id){
        Contact contact = getContact(id);
        if (contact==null){
            System.out.println("There is no contact with such #");
            return;
        }
        for (PhoneNumber phoneNumber:contact.getPhoneNumbers()){
            contactsByPhoneNumber.remove(phoneNumber.getDigits());
        }
        contacts.remove(contact);
        StringBuilder sb = new StringBuilder();
        sb.append("Contact # ").append(id).append(" has been removed");
        System.out.println(sb);
    }

    public int searchByPhoneNumber(String phoneNumber){
        Contact contact = contactsByPhoneNumber.get(phoneNumber);
        if (contact==null){
            System.out.println("\nThere is no contact with such phone number");
            return 0;
        } else {
            contact.print();
            return 1;
        }
    }

    public int searchByName(String argument, NameType type){
        int counter = 0;
        for (Contact contact:contacts){
            String name;
            switch (type){
                case FIRST: name = contact.getFirstName();
                    break;
                case LAST: name = contact.getLastName();
                    break;
                default: name = "";
            }
            if (name.toLowerCase().indexOf(argument.toLowerCase())!=-1){
                contact.print();
                counter++;
            }
        }
        printSearchStatistics(counter);
        return counter;
    }

    public int searchByAge(int from, int to){
        int counter = 0;
        for (Contact contact:contacts){
            int age = contact.getAge();
            if (from<=age && age<=to){
                contact.print();
                counter++;
            }
        }
        printSearchStatistics(counter);
        return counter;
    }

    public void print(){
        if (!sorted){
            Comparator<Contact> comparator = new Comparator<Contact>() {
                @Override
                public int compare(Contact o1, Contact o2) {
                    return o1.getFullName().compareTo(o2.getFullName());
                }
            };
            Collections.sort(contacts,comparator);
        }
        for (Contact contact:contacts){
            contact.print();
        }
    }

    private void printSearchStatistics(int counter){
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(counter).append(" contact").
                append(counter==1?" was":"s were").append(" found");
        System.out.println(sb);
    }

    public enum NameType {
        FIRST, LAST
    }
}


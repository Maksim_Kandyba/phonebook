import phonebook.data.Data;
import phonebook.userInterface.Menu;
import phonebook.userInterface.MenuA;
import phonebook.userInterface.MenuB;
import phonebook.userInterface.QuitMenu;

import java.io.*;
import java.util.Scanner;

/**
 * Created by max on 20.02.16.
 */
public class PhoneBook {
    private static final String dataFileName = "data.bin";
    private static final String passwordFileName = "password.bin";

    public static void main(String[] args) {
        if (!authorizeUser()) return;
        Data data = loadData();
        Menu.setData(data);
        if (data.getNumberOfContacts()!=0) Menu.addNext(new MenuA());
        Menu.addNext(new MenuB());
        Menu.addNext(new QuitMenu());
        Menu.mainCycle();
        saveData(data);
    }

    private static boolean authorizeUser() {
        File file = new File(passwordFileName);
        String password;
        try(
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            password = (String) ois.readObject();
        } catch (IOException e) {
            System.out.println("Can't load password from file");
            return false;
        } catch (ClassNotFoundException e) {
            System.out.println("Something wrong with 'String' class :)");
            return false;
        }
        Scanner scanner = new Scanner(System.in);
        if (password.compareTo("first launch of phone book")==0){
            System.out.println("Assign password:");
            password = scanner.nextLine();
            try(
                    FileOutputStream fos = new FileOutputStream(file);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
            ) {
                oos.writeObject(password);
                System.out.println("Password was assigned");
            } catch (IOException e) {
                System.out.println("Can't save password to file");
            }
            return true;
        }
        System.out.println("Enter password:");
        String string = scanner.nextLine();
        if (string.compareTo(password)==0) return true;
        else {
            System.out.println("Wrong password");
            return false;
        }
    }

    private static void saveData(Data data){
        File file = new File(dataFileName);
        try(
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ) {
            oos.writeObject(data);
        } catch (IOException e) {
            System.out.println("Can't save data to file");
        }
    }

    private static Data loadData(){
        File file = new File(dataFileName);
        Data data;
        try(
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            data = (Data) ois.readObject();
        } catch (IOException e) {
            System.out.println("Can't load data from file");
            data = new Data();
        } catch (ClassNotFoundException e) {
            System.out.println("Incorrect format of data");
            data = new Data();
        }
        return data;
    }
}
